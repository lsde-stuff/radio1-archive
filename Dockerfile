FROM python:alpine
USER root
RUN apk add --no-cache ffmpeg tzdata
RUN adduser -u 1000 -D -h /tmp user
ENV TZ=Europe/Prague
USER user
COPY radio1-archive.py requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
VOLUME /data
ENTRYPOINT ["python", "-u", "radio1-archive.py", "/data"]
