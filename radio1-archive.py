#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
import ffmpeg
from datetime import datetime
import sys
import json
from os import path

if len(sys.argv) > 1:
    directory = sys.argv[1]
else: directory = '.'

while True:
    stream_url = 'http://icecast2.play.cz/radio1-192.mp3'
    r = requests.get('https://www.radio1.cz')
    soup = BeautifulSoup(r.text, 'html.parser')
    data = soup.find('div', {'class': 'playingRightNow'}).td.find_all('strong')
    name = data[0].get_text()
    when = data[1].get_text()
    today = datetime.now().strftime('%Y-%m-%d')
    time_to = datetime.strptime('{0} {1}'.format(today, when.split('-')[1].strip()), '%Y-%m-%d %H:%M')
    duration = time_to - datetime.now()
    output = '{0}/{1}_{2}.mp3'.format(directory, time_to, name).replace(' ', '_')

    if path.exists(output):
        print(json.dumps({'error': '{0} already exists!'.format(output)}))
    else:
        print(json.dumps({'time': str(datetime.now()), 'output': output, 'duration': str(duration), 'error':''}, ensure_ascii=False))
        stream = ffmpeg.input(stream_url)
        stream = stream.output(output, c='copy', to=duration)
        stream = stream.global_args('-hide_banner', '-loglevel', 'quiet', '-n')
        ffmpeg.run(stream)
